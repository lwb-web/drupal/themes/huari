let gulp = require('gulp'),
  sass = require('gulp-sass'),
  sassLint = require('gulp-sass-lint')
del = require('del'),
  sourcemaps = require('gulp-sourcemaps'),
  cleanCss = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer');

const paths = {
  scss: {
    src: './scss/style.scss',
    dest: './assets/css',
    watch: './scss/**/*.scss'
  },
  icons: {
    src: './node_modules/ionicons-css/dist/**/*',
    dest: './assets/ionicons',
  },
  js: {
    bootstrap: './node_modules/bootstrap/dist/js/bootstrap.js',
    bootstrap_min: './node_modules/bootstrap/dist/js/bootstrap.min.js',
    bootstrap_map: './node_modules/bootstrap/dist/js/bootstrap.min.js.map',
    jquery: './node_modules/jquery/dist/jquery.js',
    jquery_min: './node_modules/jquery/dist/jquery.min.js',
    jquery_map: './node_modules/jquery/dist/jquery.min.map',
    popper: './node_modules/popper.js/dist/umd/popper.js',
    popper_min: './node_modules/popper.js/dist/umd/popper.min.js',
    popper_map: './node_modules/popper.js/dist/umd/popper.min.js.map',
    barrio: '../../contrib/bootstrap_barrio/js/barrio.js',
    dest: './assets/js'
  }
}

// Compile sass into CSS & auto-inject into browsers
function styles() {
  return gulp.src([paths.scss.src])
    .pipe(sourcemaps.init())
    .pipe(sassLint({
      configFile: '.sass-lint.yml'
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer({
      browsers: [
        'Chrome >= 35',
        'Firefox >= 38',
        'Edge >= 12',
        'Explorer >= 10',
        'iOS >= 8',
        'Safari >= 8',
        'Android 2.3',
        'Android >= 4',
        'Opera >= 12']
    })]))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.scss.dest))
}

// Compile sass into CSS & auto-inject into browsers
function stylesMin() {
  return gulp.src([paths.scss.src])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer({
      browsers: [
        'Chrome >= 35',
        'Firefox >= 38',
        'Edge >= 12',
        'Explorer >= 9',
        'iOS >= 8',
        'Safari >= 8',
        'Android 2.3',
        'Android >= 4',
        'Opera >= 12']
    })]))
    .pipe(sourcemaps.write())
    .pipe(cleanCss())
    .pipe(gulp.dest(paths.scss.dest))
}


// Move the javascript files into our js folder
function js() {
  return gulp.src([
    paths.js.bootstrap,
    paths.js.bootstrap_min,
    paths.js.bootstrap_map,
    paths.js.jquery,
    paths.js.jquery_min,
    paths.js.jquery_map,
    paths.js.popper,
    paths.js.popper_min,
    paths.js.popper_map,
    paths.js.barrio
  ])
    .pipe(gulp.dest(paths.js.dest))
}
// Move the javascript files into our js folder
function icons() {
  return gulp.src([
    paths.icons.src
  ])
    .pipe(gulp.dest(paths.icons.dest))
}


gulp.task('clear', () => {
  return del([
    paths.scss.dest + '/*',
    paths.icons.dest + '/*',
    paths.js.dest + '/*'
  ]);
});

gulp.task('build', gulp.series([
  gulp.parallel([
    stylesMin,
    js,
    icons,
  ])
]));

gulp.task('build-dev', gulp.series([
  gulp.parallel([
    styles,
    js,
    icons,
  ])
]));


gulp.task('watch', () => {
  gulp.watch(paths.scss.watch, (done) => {
    gulp.series(['clear', styles, js])(done);
  });
});

