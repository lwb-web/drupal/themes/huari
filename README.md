# Huari
Theme für LWB-Applikationen. Es basiert auf dem [Bootstrap Barrio Theme](https://www.drupal.org/project/bootstrap_barrio).

## Build
- npm install --global gulp-cli
- npm install
- npm run build-dev ODER npm run build
